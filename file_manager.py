# Your code goes here.
import json


def count(filename) -> int:
    """Cherche le nombre de film dans la liste"""
    with open(filename, "r", encoding="utf-8") as file:
        list_of_films = json.load(file)
        number_of_films = len(list_of_films)
        print(number_of_films)
    return number_of_films


def written_by(filename, writer) -> list:
    """Cherche les enregistrements écrits par Vince Gilligan"""
    result=[]
    with open(filename, "r", encoding="utf-8") as file: #ouvrir film.JSON stocké dans file
        film = json.load(file)
        for i in range(len(film)):
            if film[i]['Writer'] == writer:
                result.append(film[i])
                print(film[i]['Title'])  # affiche une liste de film.JSON
    return result


def longest_title(filename) -> dict:
    """Cherche le film au titre le plus long"""
    nbr_max = 0
    result={}
    with open(filename, "r", encoding="utf-8") as file:
        table_3 = json.load(file)
        for i in range(len(table_3)):
            if len(table_3[i]['Title']) > nbr_max:
                result = table_3[i]
                nbr_max = len(table_3[i]['Title'])
    return result


def best_rating(filename) -> dict:
    """Cherche le film le mieux noté"""
    result=None
    last_rating=0
    with open(filename, "r", encoding="utf-8") as file:
        table_4 = json.load(file)
        for i in range(len(table_4)):
            try:
                rating = float(table_4[i]['imdbRating'] or -1)  #-1 sert à se débarasser des valeurs indésirables
                if rating > last_rating:
                    last_rating = float(table_4[i]['imdbRating'])
                    result = table_4[i]
            except Exception:
                print("")
    print(result['Title'])
    return result
